def comienzo(dic):
    terminar = 1
    while terminar != 0:
        print("1.Si usted desea editar el diccionario presione 'e'")
        print("2.Si usted desea agregar un elemento presione 'a'")
        print("3.Si usted desea consultar presione 'c'")
        print("4.Si usted desea eliminar un valor presione 'E'")
        print("5.SI usted desea  imprimir el diccionario presione 'i'")
        accion = str(input("\nSu eleccion es : "))
        if accion == 'e':
            dic = editar(dic)
        elif accion == 'a':
            dic = agregar(dic)
        elif accion == 'c':
            consulta(dic)
        elif accion == 'E':
            dic = eliminarv(dic)
        elif accion == 'i':
            imprimir(dic)
        else:
            print("Debia escribir una de letra escogidas")
        final = str(input("\nDesea finalizar el programa S/N: "))    
        if final == 'S':
            break  
    return dic   
def agregar(a):
    llave = str(input("Nombre a la nueva llave: "))
    valor = str(input("Ingrese el valor de la llave: "))
    a[f"{llave}"] = f"{valor}"
    return a
def editar(e):
    editar = str(input("Ingrese la llave a editar: "))
    cambio = str(input("Ingrese el nuevo valor: "))
    e[f"{editar}"] = f"{cambio}" 
    return e
def consulta(c):
    consulta = str(input("Ingrese la llave a consultar: "))
    if f"{consulta}" in c :
        print (c[f"{consulta}"])
def eliminarv(E):
    cambio = str(input("Ingrese el valor que quiere eliminar: "))
    E.get(f"{cambio}") 
    E [f"{cambio}"]
    del E [f"{cambio}"]   
    return E
def imprimir(dic):
    for k,v in dic.items():
        print(k,":",v)
dic = {"6S1A":"The structural basis for signal promiscuity in a bacterial chemoreceptor","6S18":"The structural basis for signal promiscuity in a bacterial chemoreceptor","6S87":"Crystal structure of 2-methylcitrate synthase (PrpC) from Pseudomonas aeruginosa in complex with oxaloacetate.","6T27":"Structure of Human Aldose Reductase Mutant L301A with a Citrate Molecule Bound in the Anion Binding Pocket"}
comienzo(dic)
